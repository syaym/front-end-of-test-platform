import Vue from "vue";
import VueRouter from "vue-router";
import home from "@/views/home";
import caseList from "../views/cases/caseList.vue";
import casesReview from "@/views/cases/casesReview";
import casesStatistics from "@/views/cases/casesStatistics";
import login from "@/views/login";
import consumer from "@/views/consumer";
import userinfo from "@/views/consumer/info/userinfo";
import usermanage from "@/views/project/usermanage";
import author from "@/views/project/regard/author";
import version from "@/views/project/regard/version";
import projectlist from "@/views/project/project/projectlist";
import env from "@/views/project/project/env";
import secret from "@/views/consumer/secret";
import record from "@/views/consumer/record";
import apiDebug from "@/views/interfacetest/api-debug";
import apiDef from "@/views/interfacetest/api-def";
import scenario from "@/views/scenario/scenario";
import scenariomodule from "@/views/scenariomodule";
import report from "@/views/report";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    component: login,
  },
  {
    path: "/home",
    component: home,
    children: [
      { path: "caseslist", component: caseList },
      { path: "casesreview", component: casesReview },
      { path: "casesstatistics", component: casesStatistics },
      { path: "scenario/:id", component: scenario },
      { path: "report/:id", component: report },
      { path: "statistics", component: scenariomodule },
      {
        path: "api-debug",
        component: apiDebug,
      },
      {
        path: "api-def",
        component: apiDef,
      },
      {
        path: "consumer",
        component: consumer,
        children: [
          // 个人信息
          { path: "user-info", component: userinfo },
          // 人员管理
          { path: "usermanage", component: usermanage },
          // 关于作者
          { path: "author", component: author },
          { path: "version", component: version },
          { path: "project", component: projectlist },
          { path: "env", component: env },
          { path: "secret", component: secret },
          { path: "record", component: record },
        ],
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});
router.beforeEach((to, from, next) => {
  const token = JSON.parse(window.localStorage.getItem("user"));
  if (to.path !== "/login") {
    if (token) {
      next();
    } else {
      next("/login");
    }
  } else {
    next();
  }
});

// const originalPush = router.prototype.push
// //修改原型对象中的push方法
// router.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// }
export default router;
