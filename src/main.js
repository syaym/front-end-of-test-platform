import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './css/global.css'
import store from './util/anVuex'


import JsonViewer from 'vue-json-viewer'
Vue.config.productionTip = false
Vue.use(JsonViewer)

new Vue({
  store,
  router,
  render: h => h(App)

}).$mount('#app')
