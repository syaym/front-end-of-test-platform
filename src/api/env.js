import request from "../util/requests";
export default {
  addEnv(data) {
    return request({
      method: "post",
      url: "/ants/environment",
      data: data,
    });
  },
  putEnv(data) {
    return request({
      method: "put",
      url: "/ants/environment",
      data: data,
    });
  },
  deleteEnv(environmentId) {
    return request({
      method: "delete",
      url: `/ants/environment/${environmentId}`,
    });
  },
  getEnv(environmentId) {
    return request({
      method: "get",
      url: `/ants/environment/${environmentId}`,
    });
  },
  getEnvBatchByProjectId(projectId) {
    return request({
      method: "get",
      url: `/ants/environment/all-environment/${projectId}`,
    });
  },
  getDBByEnvironmentId(environmentId) {
    return request({
      method: "get",
      url: `/ants/environment/db/${environmentId}`,
    });
  },
  // 增加连接池
  addEnvironmentDB(data) {
    return request({
      method: "post",
      url: `/ants/environment/db/`,
      data: data,
    });
  },
  // 删除连接池
  putEnvironmentDB(data) {
    return request({
      method: "put",
      url: `/ants/environment/db/`,
      data: data,
    });
  },
};

export const getDBByEnvironmentId = (environmentId) => {
  return request({
    method: "get",
    url: `/ants/environment/db/${environmentId}`,
  });
};

export const getEnvBatchByProjectId = (projectId) => {
  return request({
    method: "get",
    url: `/ants/environment/all-environment/${projectId}`,
  });
};

export const addEnvironmentDB = (data) => {
  return request({
    method: "post",
    url: `/ants/environment/db/`,
    data: data,
  });
};
