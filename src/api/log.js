import request from '../util/requests'

export const alllogs = () => {
    return request({
        method: 'get',
        url: '/ants/logs'
    })
}