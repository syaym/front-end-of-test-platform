import requests from '@/util/requests'
export default {
    getNextModule(projectId) {
        return requests({
            method: 'get',
            url: `/ants/module/next-module/${projectId}`,
        })
    },
    addModule(module) {
        return requests({
            method: 'post',
            url: `/ants/module`,
            data: module
        })
    },
    putModule(module) {
        return requests({
            method: 'put',
            url: `/ants/module`,
            data: module
        })
    },
    deleteModule(moduleId) {
        return requests({
            method: 'delete',
            url: `/ants/module/${moduleId}`
        })
    }
}