import request from "../../util/requests";

export const scenariorun = (data) => {
  return request({
    method: "post",
    url: "/ants/scenario/run",
    data: data,
  });
};

export const getdolocation = () => {
  return request({
    method: "get",
    url: "/ants/api/other/do-location",
  });
};

export const getassertmode = () => {
  return request({
    method: "get",
    url: "/ants/api/other/assert-mode",
  });
};

export const getextractmode = () => {
  return request({
    method: "get",
    url: "/ants/api/other/extract-mode",
  });
};

export const searchScenarioByModule = (moduleid) => {
  return request({
    method: "get",
    url: `/ants/scenario/search/${moduleid}`,
  });
};

export const getscenariobyid = (id) => {
  return request({
    method: "get",
    url: `/ants/scenario/${id}`,
  });
};

export const saveScenario = (data) => {
  return request({
    method: "post",
    url: `/ants/scenario`,
    data: data,
  });
};

export const updateScenario = (data) => {
  return request({
    method: "put",
    url: `/ants/scenario`,
    data: data,
  });
};
