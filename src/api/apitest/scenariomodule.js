import request from '../../util/requests'
//获取当前工程下的接口场景树
export const getScenarioModule = (projectid) => {
    return request(
        {
            method: 'get',
            url: `/ants/scenario/module/tree/${projectid}`
        }
    )
}

export const getScenarioModuleById = (scenariomodule) => {
    return request(
        {
            method: 'get',
            url: `/ants/scenario/module/${scenariomodule}`
        }
    )
}

export const getNextScenarioModule = (scenarioid) => {
    return request({
        method: 'get',
        url: `/ants/scenario/module/next/${scenarioid}`
    })
}

export const createScenarioModule = (data) => {
    return request({
        method: 'post',
        url: '/ants/scenario/module',
        data: data
    })
}

export const putScenarioModule = (data) => {
    return request({
        method: 'put',
        url: '/ants/scenario/module',
        data: data
    })
}
export const deleteScenarioModule = (id) => {
    return request({
        method: 'delete',
        url: `/ants/scenario/module/${id}`,
    })
}