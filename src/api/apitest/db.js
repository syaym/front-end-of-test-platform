import request from '../../util/requests'

export const dbdebug = (data) => {

    return request({
        method: 'post',
        url: '/ants/api/db/debug',
        data
    })
}