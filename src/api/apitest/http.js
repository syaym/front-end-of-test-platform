import request from '../../util/requests'

export const getBodyTypes = function() {
    return request({
        method:'get',
        url:'/ants/api/other/body-type'
    })
}

export const getAllHttpMethod = function(){
    return request({
        method:'get',
        url:'/ants/api/other/http-method'
    })
}


export const httpdeugtest = function(data){
    return request({
        method:'post',
        url:'/ants/api/debug',
        data:data
    })
}