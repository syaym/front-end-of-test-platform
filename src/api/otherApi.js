import requests from '@/util/requests'
import { baseUrl } from "@/util/requests.js";
export default {
    downloadCaseExcel(caselist) {
        return requests({
            method: 'post',
            url: `/ants/other/batch/caseDownload`,
            data: caselist,
            responseType: 'blob'
        })
    },
    getCaseLevel() {
        return requests({
            method: 'get',
            url: `/ants/other/case-level`,
        })
    }
}
export const caseActionUploadUrlByExcel = baseUrl + "/ants/other/batch/excelCaseUpload"
export const caseActionUploadUrlByXmind = baseUrl + "/ants/other/batch/xmindCaseUpload"