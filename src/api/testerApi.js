import requests from '@/util/requests'
export default {
    getTesterInfo(testerid) {
        return requests({
            method: 'get',
            url: `/ants/tester/${testerid}`,
        })
    },
    getAllTester() {
        return requests({
            method: 'get',
            url: `/ants/tester/all-tester`,
        })
    },
    adduser(data) {
        return requests({
            method: 'post',
            url: `/ants/tester`,
            data: data
        })
    },
    putUser(data) {
        return requests({
            method: 'put',
            url: `/ants/tester`,
            data: data
        })
    },
    deleteUser(id) {
        return requests({
            method: 'delete',
            url: `/ants/tester/${id}`
        })
    }
}

export const getTesterInfo = (testerid) => {
    return requests({
        method: 'get',
        url: `/ants/tester/${testerid}`,
    })
}

export const getalltester = () => {
    return requests({
        method: 'get',
        url: '/ants/tester/all-tester'
    })
}