import request from "../../util/requests";
export default {
  // 校验数据库
  validateDB(environmentDB) {
    return request({
      method: "post",
      url: "/ants/validate/db",
      data: environmentDB,
    });
  },
};

export const validateDB = (environmentDB) => {
  return request({
    method: "post",
    url: "/ants/validate/db",
    data: environmentDB,
  });
};
