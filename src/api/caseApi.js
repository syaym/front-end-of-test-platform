import request from '../util/requests'
export default {
    add(data) {
        return request({
            method: 'post',
            url: '/ants/case',
            data: data
        })
    },
    // 分页数据
    getCasePageData(queryObject) {
        return request({
            method: 'post',
            url: '/ants/case/search',
            data: queryObject
        })
    },
    deleteCaseById(caseId) {
        return request({
            method: 'delete',
            url: `/ants/case/${caseId}`
        })
    },
    putCaseById(caseinfo) {
        return request({
            method: 'put',
            url: '/ants/case',
            data: caseinfo
        })
    }
}
