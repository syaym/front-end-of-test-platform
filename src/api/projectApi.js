import requests from '@/util/requests'
export default {
    gerAllProject() {
        return requests({
            method: 'get',
            url: `/ants/project/all-project`,
        })
    }
}