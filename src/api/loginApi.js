import request from '../util/requests'
export default {
    login(data) {
        return request({
            method: 'post',
            url: '/ants/login',
            data: data
        })
    },
    loginout() {
        return request({
            method: 'get',
            url: '/ants/login-out'
        })
    }
}