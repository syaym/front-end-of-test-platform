export const Env = class EnvironmentObejct {
    constructor(name, code, baseUrl, projectId) {
        this.name = name
        this.code = code
        this.baseUrl = baseUrl
        this.projectId = projectId
    }
}


export const EnvDB = class EnvironmentDBObject {
    constructor(code, name, username, password, jdbcUrl, environmentId) {
        this.code = code
        this.name = name
        this.username = username
        this.password = password
        this.jdbcUrl = jdbcUrl
        this.environmentId = environmentId
    }
}