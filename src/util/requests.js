// 封装axios
import axios from 'axios'
import { Message } from 'element-ui'
// 配置url
export const baseUrl = 'http://localhost:8080'

const axiosInstance = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    timeout: 8000,
    headers: {
        "Content-Type": "application/json"
    }
})
axiosInstance.interceptors.request.use(config => {
    const token = window.localStorage.getItem('user')
    if (token) {
        config.headers.authorization = token
    }
    // for (let i in config.data) {
    //     if (!config.data[i]) {
    //         delete config.data[i]
    //     }
    // }
    return config
}, error => {

})
axiosInstance.interceptors.response.use(res => {
    if (res.status >= 200 && res.status < 300) {
        return res
    } else {
        if (res.data.code === 500) {
            Message({
                showClose: true,
                message: '网络错误',
                type: 'error'
            })
        }
    }
})
export default axiosInstance
