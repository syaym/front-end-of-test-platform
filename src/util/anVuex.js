import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
    state: { username: 'admin', currentProjectId: "1418566915797815298" },
    mutations: {
        putuser(state, username) {
            state.username = username
        },
        switchProject(state, projectid) {
            state.currentProjectId = projectid
        }
    },
    actions: {}
})


export default store
//Vuex中的核心概念
// 1.state
// 提供唯一的公共数据源，共享数据放入改state中
// 1.组件中访问公共数据源使用this.$store.state.全局数据名称 如this.$store.state.count
// 2.第二种方式从vuex中导入mapstate函数 import {mapState} from 'Vuex',通过mapState隐射为组件的computed计算属性
// 如: 访问conut属性computed: {
//     ...mapState(['count'])
// }
// 2.mutation
// 1.用于改变全局属性如：
// mutations: {
//             add(state,arg) {
//                 state.count++
//             }
//         },、
// 组件中访问该方法可以通过this.$store.commit('add') add是方法名
// 组件中访问该方法可以通过this.$store.commit('add',参数) 参数对应方法名中的arg
//          2.第二种访问moutions中的方法
//          导入mapMutations import { mapMutations } from 'vuex'
//          讲方法结构到组件中的methods中
//          methods: {
//               ...mapMutations(['add','方法名2'])
//           }
//      moutations中不要执行异步操作，要执行异步操作，要在actions中如：

// 3.actions
        // actions: {
        //     addASync(mutations){
        //         setTimeout(() => {
        //             mutations.commit(方法名)
        //         }, timeout);
        //     }
        // }
        // 调用通过this.$store.dispatch(异步函数名)
        // 1调用通过this.$store.dispatch(异步函数名,参数名)
        // 2调用
        // methods: {
        //               ...maActions(['addASync','方法名2'])
        //               }
// 4.Getter 类似组件中的computed
// 1.对state中的数据进行包装，如
// getters: {
//     // 包装count
//     showNum: state = > {
//         return state.count++
//     }
// }
        // 1获取该参数this.$store.getters.名称
        // 2.第二种调用
        // 在组件中导入 import {mapGetters} from 'vuex'
        // computed: {
        //     ...mapGetters(['showNum']) showNum为getters中定义的方法
        // }
//访问vuex全局变量
    // 1.this.$store.state.变量名
//访问vuex方法
    // 1.this.$store.commit(方法名，参数)
