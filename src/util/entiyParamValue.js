
const assertmodes = {
    JSON: "JSON断言",
    RE: "正则表达式",
    TABLE: "表格断言",
    KVLISTBYKEY: "表达式断言"
}

const actionlocations = {
    RES_HEADER: "响应头",
    RES_BODY: '响应体',
    REQ_HEADER: '请求头',
    DB_TABLE: "数据库表"
}


const extractmodes = {
    JSONPATH: 'JSONPath提取',
    REGULAR: '正则表达式提取',
    XPATH: 'xpath提取',
    KVLISTBYKEY: '关键字提取'

}
//获取方言方式
const getassertmodebymode = function (mode) {
    return assertmodes[mode]
}

//获取操作位置
const getactionlocationbymode = (mode) => {
    return actionlocations[mode]
}



//获取提取方式
const getextractbymode = (mode) => {
    return extractmodes[mode]
}



export { getactionlocationbymode, getextractbymode, getassertmodebymode }