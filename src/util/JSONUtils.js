const JsonUtils = {
    propertyToStringOrToJson: function (list, properName, doway) {
        for (let i = 0; i < list.length; i++) {
            if (doway === "toString") {
                list[i][properName] = JSON.stringify(list[i][properName])
            } else if (doway === "toJSON") {
                list[i][properName] = JSON.parse(list[i][properName])
            }
        }
    },
}
export default JsonUtils