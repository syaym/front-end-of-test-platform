# Summary

Date : 2022-05-11 17:20:52

Directory /Users/moka/code/front-end-of-test-platform

Total : 80 files,  33626 codes, 233 comments, 168 blanks, all 34027 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 2 | 27,755 | 0 | 2 | 27,757 |
| Vue.js | 46 | 5,126 | 137 | 77 | 5,340 |
| JavaScript | 27 | 631 | 95 | 77 | 803 |
| YAML | 1 | 64 | 0 | 1 | 65 |
| Markdown | 1 | 19 | 0 | 6 | 25 |
| HTML | 1 | 16 | 1 | 1 | 18 |
| CSS | 1 | 8 | 0 | 0 | 8 |
| Docker | 1 | 7 | 0 | 4 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 80 | 33,626 | 233 | 168 | 34,027 |
| ops | 2 | 71 | 0 | 5 | 76 |
| public | 1 | 16 | 1 | 1 | 18 |
| src | 72 | 5,757 | 232 | 153 | 6,142 |
| src/api | 13 | 358 | 5 | 29 | 392 |
| src/api/apitest | 4 | 109 | 1 | 19 | 129 |
| src/api/otherApi | 1 | 17 | 1 | 2 | 20 |
| src/components | 13 | 586 | 10 | 19 | 615 |
| src/components/environementdb | 1 | 136 | 6 | 2 | 144 |
| src/components/report | 12 | 450 | 4 | 17 | 471 |
| src/components/report/components | 7 | 221 | 0 | 7 | 228 |
| src/css | 1 | 8 | 0 | 0 | 8 |
| src/plugins | 2 | 44 | 12 | 12 | 68 |
| src/router | 1 | 82 | 8 | 4 | 94 |
| src/test | 1 | 6 | 0 | 0 | 6 |
| src/util | 6 | 100 | 70 | 24 | 194 |
| src/valueObject | 1 | 18 | 0 | 2 | 20 |
| src/views | 32 | 4,528 | 127 | 53 | 4,708 |
| src/views/cases | 3 | 670 | 29 | 6 | 705 |
| src/views/consumer | 4 | 146 | 0 | 1 | 147 |
| src/views/consumer/info | 1 | 14 | 0 | 0 | 14 |
| src/views/home | 2 | 133 | 0 | 1 | 134 |
| src/views/interfacetest | 7 | 862 | 7 | 20 | 889 |
| src/views/interfacetest/components | 3 | 157 | 2 | 9 | 168 |
| src/views/project | 5 | 922 | 20 | 2 | 944 |
| src/views/project/project | 2 | 540 | 14 | 2 | 556 |
| src/views/project/regard | 2 | 54 | 0 | 0 | 54 |
| src/views/report | 1 | 14 | 0 | 2 | 16 |
| src/views/scenario | 6 | 1,356 | 65 | 18 | 1,439 |
| src/views/scenario/components | 5 | 1,077 | 27 | 7 | 1,111 |
| src/views/scenariomodule | 3 | 328 | 4 | 3 | 335 |
| src/views/scenariomodule/components | 2 | 274 | 4 | 2 | 280 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)