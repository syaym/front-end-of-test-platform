# Details

Date : 2022-05-11 17:20:52

Directory /Users/moka/code/front-end-of-test-platform

Total : 80 files,  33626 codes, 233 comments, 168 blanks, all 34027 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 19 | 0 | 6 | 25 |
| [babel.config.js](/babel.config.js) | JavaScript | 5 | 0 | 1 | 6 |
| [ops/Deployment.yaml](/ops/Deployment.yaml) | YAML | 64 | 0 | 1 | 65 |
| [ops/Dockerfile](/ops/Dockerfile) | Docker | 7 | 0 | 4 | 11 |
| [package-lock.json](/package-lock.json) | JSON | 27,697 | 0 | 1 | 27,698 |
| [package.json](/package.json) | JSON | 58 | 0 | 1 | 59 |
| [public/index.html](/public/index.html) | HTML | 16 | 1 | 1 | 18 |
| [src/App.vue](/src/App.vue) | Vue.js | 12 | 0 | 5 | 17 |
| [src/api/apitest/db.js](/src/api/apitest/db.js) | JavaScript | 8 | 0 | 2 | 10 |
| [src/api/apitest/http.js](/src/api/apitest/http.js) | JavaScript | 20 | 0 | 4 | 24 |
| [src/api/apitest/scenario.js](/src/api/apitest/scenario.js) | JavaScript | 38 | 0 | 9 | 47 |
| [src/api/apitest/scenariomodule.js](/src/api/apitest/scenariomodule.js) | JavaScript | 43 | 1 | 4 | 48 |
| [src/api/caseApi.js](/src/api/caseApi.js) | JavaScript | 30 | 1 | 1 | 32 |
| [src/api/env.js](/src/api/env.js) | JavaScript | 74 | 2 | 4 | 80 |
| [src/api/log.js](/src/api/log.js) | JavaScript | 7 | 0 | 1 | 8 |
| [src/api/loginApi.js](/src/api/loginApi.js) | JavaScript | 16 | 0 | 0 | 16 |
| [src/api/moduleApi.js](/src/api/moduleApi.js) | JavaScript | 29 | 0 | 0 | 29 |
| [src/api/otherApi.js](/src/api/otherApi.js) | JavaScript | 20 | 0 | 0 | 20 |
| [src/api/otherApi/validateApi.js](/src/api/otherApi/validateApi.js) | JavaScript | 17 | 1 | 2 | 20 |
| [src/api/projectApi.js](/src/api/projectApi.js) | JavaScript | 9 | 0 | 0 | 9 |
| [src/api/testerApi.js](/src/api/testerApi.js) | JavaScript | 47 | 0 | 2 | 49 |
| [src/components/environementdb/environmentdb.vue](/src/components/environementdb/environmentdb.vue) | Vue.js | 136 | 6 | 2 | 144 |
| [src/components/report/components/assertDetails.vue](/src/components/report/components/assertDetails.vue) | Vue.js | 60 | 0 | 1 | 61 |
| [src/components/report/components/dbrequestDetails.vue](/src/components/report/components/dbrequestDetails.vue) | Vue.js | 13 | 0 | 1 | 14 |
| [src/components/report/components/errorDetails.vue](/src/components/report/components/errorDetails.vue) | Vue.js | 11 | 0 | 1 | 12 |
| [src/components/report/components/extractDetails.vue](/src/components/report/components/extractDetails.vue) | Vue.js | 59 | 0 | 1 | 60 |
| [src/components/report/components/httprequestDetails.vue](/src/components/report/components/httprequestDetails.vue) | Vue.js | 20 | 0 | 1 | 21 |
| [src/components/report/components/httpres.vue](/src/components/report/components/httpres.vue) | Vue.js | 34 | 0 | 1 | 35 |
| [src/components/report/components/kvDetails.vue](/src/components/report/components/kvDetails.vue) | Vue.js | 24 | 0 | 1 | 25 |
| [src/components/report/dberror.vue](/src/components/report/dberror.vue) | Vue.js | 27 | 0 | 2 | 29 |
| [src/components/report/dbreport.vue](/src/components/report/dbreport.vue) | Vue.js | 31 | 0 | 2 | 33 |
| [src/components/report/httperror.vue](/src/components/report/httperror.vue) | Vue.js | 27 | 0 | 2 | 29 |
| [src/components/report/httpreport.vue](/src/components/report/httpreport.vue) | Vue.js | 52 | 0 | 2 | 54 |
| [src/components/report/report.vue](/src/components/report/report.vue) | Vue.js | 92 | 4 | 2 | 98 |
| [src/css/global.css](/src/css/global.css) | CSS | 8 | 0 | 0 | 8 |
| [src/main.js](/src/main.js) | JavaScript | 15 | 0 | 5 | 20 |
| [src/plugins/axios.js](/src/plugins/axios.js) | JavaScript | 40 | 12 | 10 | 62 |
| [src/plugins/element.js](/src/plugins/element.js) | JavaScript | 4 | 0 | 2 | 6 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 82 | 8 | 4 | 94 |
| [src/test/test.js](/src/test/test.js) | JavaScript | 6 | 0 | 0 | 6 |
| [src/util/JSONUtils.js](/src/util/JSONUtils.js) | JavaScript | 12 | 0 | 0 | 12 |
| [src/util/anVuex.js](/src/util/anVuex.js) | JavaScript | 16 | 56 | 5 | 77 |
| [src/util/entiyParamValue.js](/src/util/entiyParamValue.js) | JavaScript | 28 | 3 | 12 | 43 |
| [src/util/jspath.js](/src/util/jspath.js) | JavaScript | 0 | 2 | 0 | 2 |
| [src/util/maptolist.js](/src/util/maptolist.js) | JavaScript | 12 | 2 | 4 | 18 |
| [src/util/requests.js](/src/util/requests.js) | JavaScript | 32 | 7 | 3 | 42 |
| [src/valueObject/environmentValueObject.js](/src/valueObject/environmentValueObject.js) | JavaScript | 18 | 0 | 2 | 20 |
| [src/views/cases/caseList.vue](/src/views/cases/caseList.vue) | Vue.js | 640 | 29 | 6 | 675 |
| [src/views/cases/casesReview.vue](/src/views/cases/casesReview.vue) | Vue.js | 15 | 0 | 0 | 15 |
| [src/views/cases/casesStatistics.vue](/src/views/cases/casesStatistics.vue) | Vue.js | 15 | 0 | 0 | 15 |
| [src/views/consumer/index.vue](/src/views/consumer/index.vue) | Vue.js | 87 | 0 | 1 | 88 |
| [src/views/consumer/info/userinfo.vue](/src/views/consumer/info/userinfo.vue) | Vue.js | 14 | 0 | 0 | 14 |
| [src/views/consumer/record.vue](/src/views/consumer/record.vue) | Vue.js | 37 | 0 | 0 | 37 |
| [src/views/consumer/secret.vue](/src/views/consumer/secret.vue) | Vue.js | 8 | 0 | 0 | 8 |
| [src/views/home/antnav.vue](/src/views/home/antnav.vue) | Vue.js | 95 | 0 | 0 | 95 |
| [src/views/home/index.vue](/src/views/home/index.vue) | Vue.js | 38 | 0 | 1 | 39 |
| [src/views/interfacetest/api-debug.vue](/src/views/interfacetest/api-debug.vue) | Vue.js | 281 | 1 | 3 | 285 |
| [src/views/interfacetest/api-def.vue](/src/views/interfacetest/api-def.vue) | Vue.js | 8 | 0 | 0 | 8 |
| [src/views/interfacetest/components/headercomponent.vue](/src/views/interfacetest/components/headercomponent.vue) | Vue.js | 36 | 0 | 2 | 38 |
| [src/views/interfacetest/components/requestbody.vue](/src/views/interfacetest/components/requestbody.vue) | Vue.js | 40 | 1 | 3 | 44 |
| [src/views/interfacetest/components/responesbody.vue](/src/views/interfacetest/components/responesbody.vue) | Vue.js | 81 | 1 | 4 | 86 |
| [src/views/interfacetest/db.vue](/src/views/interfacetest/db.vue) | Vue.js | 126 | 2 | 5 | 133 |
| [src/views/interfacetest/http.vue](/src/views/interfacetest/http.vue) | Vue.js | 290 | 2 | 3 | 295 |
| [src/views/login.vue](/src/views/login.vue) | Vue.js | 97 | 2 | 0 | 99 |
| [src/views/project/project/env.vue](/src/views/project/project/env.vue) | Vue.js | 445 | 14 | 1 | 460 |
| [src/views/project/project/projectlist.vue](/src/views/project/project/projectlist.vue) | Vue.js | 95 | 0 | 1 | 96 |
| [src/views/project/regard/author.vue](/src/views/project/regard/author.vue) | Vue.js | 37 | 0 | 0 | 37 |
| [src/views/project/regard/version.vue](/src/views/project/regard/version.vue) | Vue.js | 17 | 0 | 0 | 17 |
| [src/views/project/usermanage.vue](/src/views/project/usermanage.vue) | Vue.js | 328 | 6 | 0 | 334 |
| [src/views/report/index.vue](/src/views/report/index.vue) | Vue.js | 14 | 0 | 2 | 16 |
| [src/views/scenario/components/assert.vue](/src/views/scenario/components/assert.vue) | Vue.js | 126 | 4 | 2 | 132 |
| [src/views/scenario/components/dbrequestconfig.vue](/src/views/scenario/components/dbrequestconfig.vue) | Vue.js | 218 | 3 | 1 | 222 |
| [src/views/scenario/components/extract.vue](/src/views/scenario/components/extract.vue) | Vue.js | 125 | 3 | 2 | 130 |
| [src/views/scenario/components/httprequestconfig.vue](/src/views/scenario/components/httprequestconfig.vue) | Vue.js | 444 | 0 | 1 | 445 |
| [src/views/scenario/components/scenariobaseconfig.vue](/src/views/scenario/components/scenariobaseconfig.vue) | Vue.js | 164 | 17 | 1 | 182 |
| [src/views/scenario/scenario.vue](/src/views/scenario/scenario.vue) | Vue.js | 279 | 38 | 11 | 328 |
| [src/views/scenariomodule/components/table.vue](/src/views/scenariomodule/components/table.vue) | Vue.js | 104 | 0 | 0 | 104 |
| [src/views/scenariomodule/components/tree.vue](/src/views/scenariomodule/components/tree.vue) | Vue.js | 170 | 4 | 2 | 176 |
| [src/views/scenariomodule/index.vue](/src/views/scenariomodule/index.vue) | Vue.js | 54 | 0 | 1 | 55 |
| [vue.config.js](/vue.config.js) | JavaScript | 3 | 0 | 0 | 3 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)